//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    cardCur: 0,
    swiperList: [{
      id: 0,
      type: 'image',
      url: '../../images/home_pt/pt.jpg'
    }, {
      id: 1,
      type: 'image',
      url: '../../images/home_pt/pt1.jpg',
    }, {
      id: 2,
      type: 'image',
      url: '../../images/home_pt/pt2.jpg'
    }, {
      id: 3,
      type: 'image',
      url: '../../images/home_pt/pt3.jpg'
    }, {
      id: 4,
      type: 'image',
      url: '../../images/home_pt/pt4.jpg'
    }, {
      id: 5,
      type: 'image',
      url: '../../images/home_pt/pt5.jpg'
    }, {
      id: 6,
      type: 'image',
      url: '../../images/home_pt/pt6.jpg'
    }],
    places: [{
        id: 1,
        name: "第一食堂"
      }, {
        id: 0,
        name: "第二食堂"
      },
      {
        id: 0,
        name: "第三食堂"
      },
      {
        id: 0,
        name: "第四食堂"
      }
    ],
    stops: [],
    stop1:[],stop2:[],stop3:[],stop4:[],
  },

  // places 点击样式切换 ，重新渲染
  onClick: function(e) {
    wx.vibrateShort();
    var item = e.currentTarget.dataset.item
    var places = []
    for (let i = 0; i < this.data.places.length; i++) {
      places.push({
        id: 0,
        name: this.data.places[i].name
      })
      if (this.data.places[i].name == item.name) {
        places[i].id = 1
      }
    }
    this.setData({
      ['places']: places,
    })
    // 后台数据导入： 店铺数据
    if (item.name == '第一食堂') {
      this.setData({
        ['stops']: this.data.stop1
      })
    } else if (item.name == '第二食堂') {
      this.setData({
        ['stops']: this.data.stop2
      })
    } else if (item.name == '第三食堂') {
      this.setData({
        ['stops']: this.data.stop3
      })
    } else if (item.name == '第四食堂') {
      this.setData({
        ['stops']: this.data.stop4
      })
    }
  },

  getStopsInfo:function(){
    var that = this
    wx.showLoading({
      title: 'HOBO提速中...',
    })
    wx.request({
      url: 'http://'+app.globalData.localhost+'/stall/findStallByPlace?'+'id=1',
      success:function(res){
        console.log(res)
        for (let i = 0;i< res.data.length;i++){
          that.setData({
            ['stop1']:that.data.stop1.concat({
              stallId: res.data[i].s_id,
              src:res.data[i].s_picture,
              name:res.data[i].s_name,
              price:res.data[i].s_consume,
              info:res.data[i].s_introduce,
              place:1
            })
          })
        }
        that.setData({
          ['stop1']: that.data.stop1
        })
        that.setData({
          ['stops']:that.data.stop1
        })
        wx.hideLoading();
      }
    })
    wx.request({
      url: 'http://'+app.globalData.localhost+'/stall/findStallByPlace?'+'id=2',
      success:function(res){
        for (let i = 0;i< res.data.length;i++){
          that.setData({
            ['stop2']:that.data.stop2.concat({
              stallId: res.data[i].s_id,
              src:res.data[i].s_picture,
              name:res.data[i].s_name,
              price:res.data[i].s_consume,
              info:res.data[i].s_introduce,
              place:2
            })
          })
        }
        that.setData({
          ['stop2']: that.data.stop2
        })
      }
    })
    wx.request({
      url: 'http://'+app.globalData.localhost+'/stall/findStallByPlace?'+'id=3',
      success:function(res){
        for (let i = 0;i< res.data.length;i++){
          that.setData({
            ['stop3']:that.data.stop3.concat({
              stallId: res.data[i].s_id,
              src:res.data[i].s_picture,
              name:res.data[i].s_name,
              price:res.data[i].s_consume,
              info:res.data[i].s_introduce,
              place:3
            })
          })
        }
        that.setData({
          ['stop3']: that.data.stop3
        })
      }
    })
    wx.request({
      url: 'http://'+app.globalData.localhost+'/stall/findStallByPlace?'+'id=4',
      success:function(res){
        for (let i = 0;i< res.data.length;i++){
          that.setData({
            ['stop4']:that.data.stop4.concat({
              stallId: res.data[i].s_id,
              src:res.data[i].s_picture,
              name:res.data[i].s_name,
              price:res.data[i].s_consume,
              info:res.data[i].s_introduce,
              place:4
            })
          })
        }
        that.setData({
          ['stop4']: that.data.stop4
        })
      }
    })
  },

  onLoad() {
   
    this.towerSwiper('swiperList');
    // 初始化towerSwiper 传已有的数组名即可
    this.getStopsInfo();
  },
  DotStyle(e) {
    this.setData({
      DotStyle: e.detail.value
    })
  },
  // cardSwiper
  cardSwiper(e) {
    this.setData({
      cardCur: e.detail.current
    })
  },
  // towerSwiper
  // 初始化towerSwiper
  towerSwiper(name) {
    let list = this.data[name];
    for (let i = 0; i < list.length; i++) {
      list[i].zIndex = parseInt(list.length / 2) + 1 - Math.abs(i - parseInt(list.length / 2))
      list[i].mLeft = i - parseInt(list.length / 2)
    }
    this.setData({
      swiperList: list
    })
  },
  // towerSwiper触摸开始
  towerStart(e) {
    this.setData({
      towerStart: e.touches[0].pageX
    })
  },
  // towerSwiper计算方向
  towerMove(e) {
    this.setData({
      direction: e.touches[0].pageX - this.data.towerStart > 0 ? 'right' : 'left'
    })
  },
  // towerSwiper计算滚动
  towerEnd(e) {
    let direction = this.data.direction;
    let list = this.data.swiperList;
    if (direction == 'right') {
      let mLeft = list[0].mLeft;
      let zIndex = list[0].zIndex;
      for (let i = 1; i < list.length; i++) {
        list[i - 1].mLeft = list[i].mLeft
        list[i - 1].zIndex = list[i].zIndex
      }
      list[list.length - 1].mLeft = mLeft;
      list[list.length - 1].zIndex = zIndex;
      this.setData({
        swiperList: list
      })
    } else {
      let mLeft = list[list.length - 1].mLeft;
      let zIndex = list[list.length - 1].zIndex;
      for (let i = list.length - 1; i > 0; i--) {
        list[i].mLeft = list[i - 1].mLeft
        list[i].zIndex = list[i - 1].zIndex
      }
      list[0].mLeft = mLeft;
      list[0].zIndex = zIndex;
      this.setData({
        swiperList: list
      })
    }
  },
  onShow: function(e) {
    this.setData({
      msgList: [{
          title: "【服务通知】一饭来了一批美味的阿姨，快来尝尝吧~"
        },
        {
          title: "【服务通知】二饭来了一批美味的阿姨，快来尝尝吧~"
        },
        {
          title: "【服务通知】三饭来了一批美味的阿姨，快来尝尝吧~"
        },
        {
          title: "【服务通知】四饭来了一批美味的阿姨，快来尝尝吧~"
        }
      ]
    });
  },

  // 跳转到点餐界面
  OnClickToOrder:function(e){
    console.log(e)
    wx.vibrateShort();
    var name = e.currentTarget.dataset.item.name;
    var src = e.currentTarget.dataset.item.src;
    var stall = e.currentTarget.dataset.item.stallId;
    var place = e.currentTarget.dataset.item.place;
    console.log(stall)
    wx.navigateTo({
      url: '/pages/stopinfo/stopinfo?name=' + name +"&place="+place+  "&stall=" + stall+"&src=" + src ,
      success: function(res) {
        console.log("yes")
      },
      fail: function(res) {},
      complete: function(res) {},
    })
  }
})