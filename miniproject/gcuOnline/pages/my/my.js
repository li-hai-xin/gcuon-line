// pages/my/my.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    SYSWidth: "85vw",
    //用户信息
    userInfo:{
      avatarUrl:"",//头像
      nickName:""//昵称
    },
    //在wxml中可以使用wx:for遍历，简化代码
    items: [{
      page:'order',
      src: '../../images/my_pt/shangjia.png',
      text: '商家订单'
    }, 
    {
      page:'waiter',
      src: '../../images/my_pt/kefu.png',
      text: '联系客服'
    }, 
    {
      page:'about',
      src: '../../images/my_pt/about.png',
      text: '关于我们'
    },
    {
      page:'',
      src: '../../images/my_pt/setting.png',
      text: '设置'
    }],
    boxItems:[{
      page:'myOrder',
      src:"../../images/my_pt/ding.png",
      text:"我的订单"
    },
    {
      page:'myCard',
      src: "../../images/my_pt/quan.png",
      text: "我的卡券"
    }]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    wx.getUserInfo({
      success:function(res){
        console.log(res);
        var avatarUrl = 'userInfo.avatarUrl';
        var nickName = 'userInfo.nickName';
        that.setData({
          [avatarUrl]:res.userInfo.avatarUrl,
          [nickName]:res.userInfo.nickName,
        })
      }
    })
  },
  authentication:function(){
    wx.request({
      url: 'http://'+app.globalData.localhost+'/OrderStall/Authentication?' + 'boss_id=' + app.globalData.openid,
      success:function(res){
        if(res.data){
          wx.navigateTo({
            url:'/pages/order/order'
          })
        }else{
          wx.showToast({
            title:"权限不足,您不是商家哦",
            // title:"点你妈哇，点点点,您不是商家",
            icon:"none"
          })
        }
      },
      fail:function(){
        console.log('请求失败了')
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  
})