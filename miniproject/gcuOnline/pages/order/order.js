// pages/order/order.js

const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    navbarActiveIndex: 0,
    title:["未做订单","今日订单","本月订单"],
  },

  /**
   * 导航栏点击事件
   */
  onNavBarTap: function (e) {
    
    // 获取点击的navbar的index
    let navbarTapIndex = e.currentTarget.dataset.navbarIndex
    // 设置data属性中的navbarActiveIndex为当前点击的navbar
    this.setData({
      navbarActiveIndex:navbarTapIndex
    })
    if (navbarTapIndex == 0){
      this.getOrderNotdo()
    }else if(navbarTapIndex == 1){
      this.getOrderToday()
    }else{
      this.getOrderMonth()
    }
  },
  //获取数据
  // getStopsInfo:function() {
  //   wx.request({
  //     url: 'http://'+app.globalData.localhost+'/Order/findOrderForStallNotissued?'+'stall=1',
  //     success:function(res){
  //       for(let i = 0;i<res.data.length;i++){
  //         that.setData({
  //           ['order0']:that.data.order0.concat({
  //             src:res.data[i].picture,
  //             name:res.data[i].foodName,
  //             user:p_id,
  //             num:1,
  //             total:res.data[i].total
  //           })
  //         })
  //       }
  //     }
  //   })
  // },

  getOrderNotdo:function() {
    var that = this
    wx.request({
      url: 'http://'+app.globalData.localhost+'/OrderStall/findOrdersByNumber?'+'boss_id='+app.globalData.openid,
      success:function(res){
        that.setData({
          orders:res.data
        })
      }
    })
  },
  getOrderToday:function() {
    var that = this
    wx.request({
      url: 'http://'+app.globalData.localhost+'/OrderStall/findOrderForStallToday?'+'boss_id='+app.globalData.openid,
      success:function(res){
        that.setData({
          orders:res.data
        })
      }
    })
  },
  getOrderMonth:function() {
    var that = this
    wx.request({
      url: 'http://'+app.globalData.localhost+'/OrderStall/findOrderForStallMonth?'+'boss_id='+app.globalData.openid,
      success:function(res){
        console.log(res.data)
        that.setData({
          orders:res.data
        })
      }
    })
  },
  // 售罄的事件
  cancel: function() {
    wx.showModal({
      title: "提示",
      content: "确定取消订单？",
      success: function(res) {
        if (res.confirm) {
          console.log('确定')
        } else if (res.cancel) {
          console.log('取消')
        }
      }
    })
  },
  // 出单操作
  finsh:function (e) {
    var that = this
    wx.showModal({
      title:"提示",
      content:"确认出单吗？",
      success: function (res) {
        if (res.confirm) {
         console.log('确定')
         wx.request({
           url: 'http://'+app.globalData.localhost+'/Order/upDateOrderState1?'+'number='+e.currentTarget.dataset.id,
           success:function (res) {
            that.getOrderNotdo()
           }
         })
        } else if (res.cancel) {
         console.log('取消')
        }
      }
    })
  },
  onLoad(){
    // this.getStopsInfo()
    this.getOrderNotdo()
    console.log(app.globalData.localhost)
  }
})