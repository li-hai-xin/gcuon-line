// pages/myOrder/myOrder.js
const app = getApp()
Page({
  data: {
    navbarActiveIndex: 0,
    types: [{
        type: "已订餐"
      },
      {
        type: "已出单"
      },
      {
        type: "已收货"
      },
    ]
  },
  // 订单种类切换，重新渲染
  onNavBarTap: function(e) {
    // 获取点击的navbar的index
    let navbarTapIndex = e.currentTarget.dataset.navbarIndex
    // 设置data属性中的navbarActiveIndex为当前点击的navbar
    this.setData({
      navbarActiveIndex: navbarTapIndex
    })
    if (navbarTapIndex == 0) {
      this.notissued()
    } else if (navbarTapIndex == 1) {
      this.issued()
    } else if (navbarTapIndex == 2) {
      this.received()
    } 
  },
  //取消订单
  cancel: function(e) {
    var that = this
    wx.showModal({
      title: "提示",
      content: "确定取消订单？",
      success: function(res) {
        if (res.confirm) {
          console.log('确定')
          var number = e.currentTarget.dataset.number
          wx.request({
            url: 'http://'+app.globalData.localhost+'/Order/deleteOrderState?'+'number='+number,
            success:function(){
              that.notissued()
            }
          })
        } else if (res.cancel) {
          console.log('取消')
        }
      }
    })
  },
  //未出单
  notissued:function(params) {
    var that = this
    wx.request({
      url: 'http://'+app.globalData.localhost+'/OrderPeople/findOrderForPeopleNotissued?'+'user_id='+app.globalData.openid,
      success:function(res){
        that.setData({
          Notissued:res.data
        })
        console.log(res.data)
      }
    })
  },
  //已出单
  issued:function (params) {
    var that = this
    wx.request({
      url: 'http://'+app.globalData.localhost+'/OrderPeople/findOrderForPeopleIssued?'+'user_id='+app.globalData.openid,
      success:function(res){
        that.setData({
          items:res.data
        })
      }
    })
  },
  //已收货
  received:function (params) {
    var that = this
    wx.request({
      url: 'http://'+app.globalData.localhost+'/OrderPeople/findOrderForPeopleReceived?'+'user_id='+app.globalData.openid,
      success:function(res){
        that.setData({
          Received:res.data
        })
      }
    })
  },
  //确认收货操作
  enter: function(e) {
    var that = this
    var number = e.currentTarget.dataset.number
    wx.showModal({
      title: "提示",
      content: "确认收货？",
      success: function(res) {
        if (res.confirm) {
          console.log('确认')
          wx.request({
            url: 'http://'+app.globalData.localhost+'/Order/upDateOrderState2?'+'number='+number,
            success:function(){
              that.issued()
            }
          })
        } else if (res.cancel) {
          console.log('取消')
        }
      }
    })
  },
  //加载未出单数据
  onLoad:function(options){
    // console.log(options)
    // var orders1 = []
    // if (options.src){
    //   orders1 = [{
    //     src: options.src,
    //     name: options.name,
    //     time: '2020-10-01 20:20',
    //     total: options.total
    //   }]
    // }else{
    //   orders1 = [{
    //     src: 'http://p.ananas.chaoxing.com/star3/origin/c9760dd368d0d2aed1c58d0c4767b558.jpg?rw=500&rh=375&_fileSize=34063&_orientation=1',
    //     name: '北国饭店',
    //     time: '2020-10-01 20:20',
    //     total: 250
    //   }]
    // }
    // this.setData({
    //   orders:orders1,
    //   orders1: orders1
    // })
    this.notissued()
  },
  onUnload:function(){
    wx.navigateBack({
      delta: 2
    });
  },
  //查看订单详细信息
  showOrder:function(e){
    var number = e.currentTarget.dataset.number
    var that = this
    wx.request({
      url: 'http://'+app.globalData.localhost+'/OrderPeople/detailedOrderInformation?'+'number='+number,
      success:function(res){
        that.setData({
          info:res.data
        })
      }
    })
    this.setData({
      isOrderTrue:true
    })
  },
  hideOrder:function() {
    this.setData({
      isOrderTrue:false
    })
  }
})