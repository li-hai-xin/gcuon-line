// pages/stopinfo/stopinfo.js
const app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    showNav: true,
    navHeight: null,
    navTop: null,
    windowHeight: null,
    list: [],
    TabCur: 0,
    MainCur: 0,
    VerticalNavTop: 0,
    load: true,
    total:0,              //弹出层状态
    OrderingList:[],      //订单列表
    priceTotal:0          //总价
    
  },
  
  // 右边滑动的算法
  VerticalMain(e) {
    let that = this;
    let list = this.data.list;
    let tabHeight = 0;
    if (this.data.load) {
      for (let i = 0; i < list.length; i++) {
        let view = wx.createSelectorQuery().select("#main-" + list[i].id);
        view.fields({
          size: true
        }, data => {
          list[i].top = tabHeight;
          tabHeight = tabHeight + data.height;
          list[i].bottom = tabHeight;
        }).exec();
      }
      that.setData({
        load: false,
        list: list
      })
    }
    let scrollTop = e.detail.scrollTop + 20;
    for (let i = 0; i < list.length; i++) {
      if (scrollTop > list[i].top && scrollTop < list[i].bottom) {
        that.setData({
          VerticalNavTop: (list[i].id - 1) * 50,
          TabCur: list[i].id
        })
        return false
      }
    }
  },
  // 跳转到支付页面
  topay:function(){
    console.log(this.data.stall)
    var OrderingList = encodeURIComponent(JSON.stringify(this.data.OrderingList))
    var place = this.data.place;
    var stopName = this.data.stopName
    if(this.data.OrderingList.length != 0){
      wx.navigateTo({
        url: '/pages/pay/pay?priceTotal=' + this.data.priceTotal + "&OrderingList=" + OrderingList + "&place=" + place + "&name=" + stopName+"&stall="+this.data.stall + "&src=" + this.data.stopSrc,
      })
    }else{
     wx.showToast({
       title: '请选择至少一种食品',
       icon: 'none',
       duration: 1500
     })
    }
    
  },

  getFoodByStallName(){
    var that = this
    wx.showLoading({
      title: 'HOBO提速中...',
    })
    wx.request({
      url: 'http://' + app.globalData.localhost +'/foodType/findFoodsByStall?&stall='+that.data.stall,
      success:function(res){
        console.log(res)
        var list = res.data
        that.setData({
          list: list,
          listCur: list[0]
        })
        wx.hideLoading();
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      stopSrc: options.src,
      stopName: options.name,
      stall:options.stall,
      place:options.place
    })
    var menuButtonObject = wx.getMenuButtonBoundingClientRect();
    var that = this
    wx.getSystemInfo({
      success: function(res) {
        let statusBarHeight = res.statusBarHeight,
          navTop = menuButtonObject.top, //胶囊按钮与顶部的距离
          navHeight = statusBarHeight + menuButtonObject.height + (menuButtonObject.top - statusBarHeight) * 2; //导航高度
        that.setData({
          navHeight: navHeight,
          navTop: navTop,
          windowHeight: res.windowHeight
        })
      },
    })

    this.getFoodByStallName()
  },

  // 左边滑动的算法
  tabSelect(e) {
    this.setData({
      TabCur: e.currentTarget.dataset.id,
      MainCur: e.currentTarget.dataset.id,
      VerticalNavTop: (e.currentTarget.dataset.id - 1) * 50
    })
  },

  OnClickBack: function(e) {
    wx.navigateBack({
      delta: 1
    })
  },

  // 计算总价
  CulTotal:function(value){
    var priceTotal = 0
    for (let i = 0; i < value.length; i++) {
      // 计算总价
      priceTotal = (Number(priceTotal) + Number(value[i].price)).toFixed(2)
    }
    this.setData({
      ['OrderingList']: value,
      ['priceTotal']: priceTotal
    })
  },

  // 点餐减按钮
  sub:function(e){
    console.log(e)
    wx.vibrateShort();
    var item = e.currentTarget.dataset.item
    var id = e.currentTarget.dataset.id
    var partid = e.currentTarget.dataset.partid
    var priceTotal = 0
    if (this.data.list[id].parts[partid].num > 0){
      this.setData({
        // 右边的菜品数量
        ['list[' + id + '].parts[' + partid + '].num']: this.data.list[id].parts[partid].num - 1,
        // 左边的菜品数量总和
        ['list[' + id + '].num']: this.data.list[id].num - 1
      })
      // 减少数量
      for (let i = 0; i < this.data.OrderingList.length; i++) {
        if (this.data.OrderingList[i].name == item.name) {
          this.setData({
            ['OrderingList[' + i + '].num']: this.data.OrderingList[i].num - 1,
            ['OrderingList[' + i + '].price']: (item.price * (this.data.OrderingList[i].num - 1)).toFixed(2)
          })
          // 若减到0了
          if (this.data.OrderingList[i].num == 0) {
            // 删除某一个对象元素
            this.data.OrderingList.splice(i, 1)
            console.log(this.data.OrderingList)
          }
        }
      }
      this.setData({
        ['total']: this.data.OrderingList.length
      })
    }
    this.CulTotal(this.data.OrderingList)
  },

  // 点餐加按钮
  add: function (e) {
    console.log(e)
    wx.vibrateShort();
    var item = e.currentTarget.dataset.item
    var id = e.currentTarget.dataset.id
    var partid = e.currentTarget.dataset.partid
    var priceTotal = 0
    this.setData({
      ['list[' + id + '].parts[' + partid + '].num']: this.data.list[id].parts[partid].num + 1,
      ['list[' + id + '].num']: this.data.list[id].num + 1
    })
    var flag = true
    for (let i = 0; i < this.data.OrderingList.length;i++){
      if (this.data.OrderingList[i].name == item.name){
        flag = false
        // 修改菜单
        this.setData({
          ['OrderingList[' + i + '].num']: this.data.OrderingList[i].num + 1,
          ['OrderingList[' + i + '].price']: (item.price * (this.data.OrderingList[i].num + 1)).toFixed(2)
        })
        console.log(this.data.OrderingList)
      }
      // 计算总价
      priceTotal = (Number(priceTotal) + Number(this.data.OrderingList[i].price)).toFixed(2)
    }
    // 导入菜单
    if (flag){
      this.setData({
        ['OrderingList']: this.data.OrderingList.concat({
          name: item.name,
          num: this.data.list[id].parts[partid].num,
          price: this.data.list[id].parts[partid].num * item.price,
          img:item.picture,
          id:item.id
        })
      })
      // 加上新导入的价格
      priceTotal = (Number(priceTotal) + Number(item.price)).toFixed(2)
      console.log(this.data.OrderingList)
    }
    this.setData({
      ['total']:this.data.OrderingList.length
    })
    this.CulTotal(this.data.OrderingList)
    
  },

  // 弹出层减按钮
  OrderingSub:function(e){
    wx.vibrateShort();
    // 点击到的item
    var item = e.currentTarget.dataset.item
    // 订餐列表
    var ol = this.data.OrderingList
    // 菜单列表
    var l = this.data.list
    for (let i = 0; i < ol.length; i++) {
      if (ol[i].name == item.name){
        this.setData({
          ['OrderingList[' + i + '].num']: ol[i].num - 1,
          ['OrderingList[' + i + '].price']: ((ol[i].price / ol[i].num) * (ol[i].num - 1)).toFixed(2)
        })
      }
      if (this.data.OrderingList[i].num == 0){
        this.data.OrderingList.splice(i, 1)
      }
    }
    for (let i = 0; i < l.length; i++) {
      if(l[i].num == 0)continue;
      for (let j = 0; j < l[i].parts.length; j++){
        if (l[i].parts[j].num == 0)continue;
        if (l[i].parts[j].name == item.name){
          this.setData({
            ['list['+i+'].parts['+j+'].num']: this.data.list[i].parts[j].num - 1,
            ['list[' + i + '].num']: this.data.list[i].num - 1
          })
        }
      }
    }
    this.CulTotal(this.data.OrderingList)
    if (this.data.priceTotal == 0) this.hideModal();
  },
  // 弹出层加按钮
  OrderingAdd: function (e) {
    wx.vibrateShort();
    // 点击到的item
    var item = e.currentTarget.dataset.item
    // 订餐列表
    var ol = this.data.OrderingList
    // 菜单列表
    var l = this.data.list
    for (let i = 0; i < ol.length; i++) {
      if (ol[i].name == item.name) {
        this.setData({
          ['OrderingList[' + i + '].num']: ol[i].num + 1,
          ['OrderingList[' + i + '].price']: ((ol[i].price / ol[i].num) * (ol[i].num + 1)).toFixed(2)
        })
      }
      if (this.data.OrderingList[i].num == 0) {
        this.data.OrderingList.splice(i, 1)
      }
    }
    for (let i = 0; i < l.length; i++) {
      for (let j = 0; j < l[i].parts.length; j++) {
        if (l[i].parts[j].name == item.name) {
          this.setData({
            ['list[' + i + '].parts[' + j + '].num']: this.data.list[i].parts[j].num + 1,
            ['list[' + i + '].num']: this.data.list[i].num + 1
          })
        }
      }
    }
    this.CulTotal(this.data.OrderingList)
  },
  //点击我显示底部弹出框
  clickme: function () {
    if (this.data.priceTotal != 0){
      if (app.globalData.A == 1){
        this.showModal();
      }else{
        this.hideModal();
      }
    }else{
      wx.showToast({
        title: '还没有选择食品',
        icon: 'none',
        duration: 1000
      })
    }
  },

  //显示对话框
  showModal: function () {
    // 显示遮罩层
    var animation = wx.createAnimation({
      duration: 200,
      timingFunction: "ease",
      delay: 0
    })
    this.animation = animation
    animation.translateY(300).step()
    this.setData({
      animationData: animation.export(),
      showModalStatus: true
    })
    setTimeout(function () {
      animation.translateY(0).step()
      this.setData({
        animationData: animation.export()
      })
    }.bind(this), 200)
    app.globalData.A = 2
  },
  //隐藏对话框
  hideModal: function () {
    // 隐藏遮罩层
    var animation = wx.createAnimation({
      duration: 200,
      timingFunction: "ease",
      delay: 0
    })
    this.animation = animation
    animation.translateY(300).step()
    this.setData({
      animationData: animation.export(),
    })
    setTimeout(function () {
      animation.translateY(0).step()
      this.setData({
        animationData: animation.export(),
        showModalStatus: false
      })
    }.bind(this), 200)
    app.globalData.A = 1
  }
})