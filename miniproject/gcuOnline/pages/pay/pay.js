// pages/pay/pay.js

const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },
  enterpay:function(params) {
    wx.showLoading({
      title: '生成订单...',
    })
    var that = this
    console.log(that.data.orderInfo)
    wx.request({
      method:'POST',
      url: 'http://' + app.globalData.localhost + '/Order/insertOrder?',
      data:{
        orderInfo: that.data.orderInfo
      },
      success: function (res) {
          wx.hideLoading()
          wx.showToast({
            title: '用餐愉快',
            duration: 1200
          })
          var name = that.data.name;
          var total = that.data.total
          var src = that.data.src
          setTimeout(function () {
            wx.navigateTo({
             url: '/pages/myOrder/myOrder',
            })
          }, 1200);
      },
      fail: function(res){
        console.log("插入失败！")
      }
    })
    
  },
  toShow:function(){

  },


  onLoad:function(options){
    var OrderingList = JSON.parse(decodeURIComponent(options.OrderingList))

    var orderInfo = {
      place : options.place,
      stall : options.stall,
      p_id : app.globalData.openid,
      userName : app.globalData.userInfo.nickName,
      orderingList: OrderingList
    }

    this.setData({
      total:options.priceTotal,
      orders:OrderingList,
      place:options.place,
      name:options.name,
      src:options.src,
      stall:options.stall,
      orderInfo:orderInfo
    }) 

    console.log(this.data.orderInfo)
  }
})