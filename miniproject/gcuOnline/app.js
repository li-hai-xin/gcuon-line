//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    wx.showLoading({
      title: '加载数据...',
    })


    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        var that = this
        if(res.code){
          wx.request({
            url: 'http://' + that.globalData.localhost + '/GetOpenId/onLogin?codeId='+res.code,
            success:function(res){
              that.globalData.openid = res.data
              var user_id = res.data
              wx.getUserInfo({
                success:function(res){
                  that.globalData.userInfo = res.userInfo
                  var username = res.userInfo.nickName
                  wx.request({
                    url:'http://' + that.globalData.localhost + '/user/insertUser?user_id=' + user_id + "&username=" + username,
                    success: function (res) {
                      console.log(res)
                      console.log("登录成功！")
                    },
                    fail: function (res) {
                      console.log(res)
                    }
                  })
                }
              })
              wx.hideLoading()
            }
          })
        }
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          
        }
      }
    })
   
   
  },
  globalData: {
    userInfo: null,
    A : 1,
    localhost:'www.gzhobo.design:8090'
    // localhost:'192.168.137.1:8090'
    // localhost:'localhost:8090'
  }
})