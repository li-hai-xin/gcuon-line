package gcuF.gcuonline.tools;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class MyUtil {
	//获取具体时间
	public static String getTime(){
		String time = null;
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		time = sdf.format(date);
		return time;
	}
	//获取日期
	public static String getDay(){
		String time = null;
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); 
		time = sdf.format(date);
		return time;
	}
	//获取月份
	public static String getMonth(){
		String time = null;
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM"); 
		time = sdf.format(date);
		return time;
	}
}
