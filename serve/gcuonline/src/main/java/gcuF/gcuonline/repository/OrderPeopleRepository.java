package gcuF.gcuonline.repository;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import gcuF.gcuonline.pojo.OrderDatailed;
import gcuF.gcuonline.pojo.OrderPeople;

public interface OrderPeopleRepository {
	//查看订单（用户-未出单）
	@Select("select o.number, o.book_time, o.total, s.s_name, s.s_picture "
			+ "from `order` o, stall s "
			+ "where o.stall = s.s_id and o.number = #{number}")
	public List<OrderPeople> findOrderForPeopleNotissued(String number);
	//查看订单（用户-已出单）
	@Select("select o.number, o.book_time, o.total, s.s_name, s.s_picture "
			+ "from `order` o, stall s "
			+ "where o.stall = s.s_id and o.number = #{number}")
	public List<OrderPeople> findOrderForPeopleIssued(String number);
	//查看订单（用户-已收货）
	@Select("select o.number, o.book_time, o.total, s.s_name, s.s_picture "
			+ "from `order` o, stall s "
			+ "where o.stall = s.s_id and o.number = #{number}")
	public List<OrderPeople> findOrderForPeopleReceived(String number);
	//获取订单号（未出单）
	@Select("select number from `order` "
			+ "where p_id = #{user_id} and state = 1 group by number") 
	public List<String> findNumberForPeopleNotissued(String user_id);
	//获取订单号（已出单）
	@Select("select number from `order` "
			+ "where p_id = #{user_id} and state = 2 group by number") 
	public List<String> findNumberForPeopleIssued(String user_id);
	//获取订单号（已收货）
	@Select("select number from `order` "
			+ "where p_id = #{user_id} and state = 3 group by number") 
	public List<String> findNumberForPeopleReceived(String user_id);
	//详细菜品信息
	@Select("select f.name, o.price, o.num "
			+ "from `order` o, food f "
			+ "where o.id = f.id and o.number = #{number}")
	public List<OrderDatailed> detailedOrderInformation(int number);
	
}
