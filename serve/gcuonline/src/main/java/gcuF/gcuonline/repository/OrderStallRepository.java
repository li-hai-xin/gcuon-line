package gcuF.gcuonline.repository;

import java.util.List;
import org.apache.ibatis.annotations.Select;
import gcuF.gcuonline.pojo.Orders;

public interface OrderStallRepository {
	//商家查询订单（未出单）
	@Select("select o.number, u.username, o.num, f.name, f.picture "
			+ "from `order` o, food f, `user` u "
			+ "where o.id = f.id and o.p_id = u.user_id "
			+ "and o.number = #{number}")
	public List<Orders> findOrdersByNumber(String number);
	//商家查询今日订单
	@Select("select o.number, u.username, o.num, f.name, f.picture "
			+ "from `order` o, food f, `user` u "
			+ "where o.id = f.id and o.p_id = u.user_id "
			+ "and o.number = #{number}")
	public List<Orders> findOrderForStallToday(String number);
	//商家查询本月订单
	@Select("select o.number, u.username, o.num, f.name, f.picture "
			+ "from `order` o, food f, `user` u "
			+ "where o.id = f.id and o.p_id = u.user_id "
			+ "and o.number = #{number}")
	public List<Orders> findOrderForStallMonth(String number);
	//获取订单号（未出单）
	@Select("select o.number "
			+ "from `order` o, business b "
			+ "where o.stall = b.stall_id and o.state = 1 "
			+ "and b.boss_id = #{boss_id} group by o.number") 
	public List<String> findOrders1(String boss_id);
	//获取订单号（今日订单）
	@Select("select o.number "
			+ "from `order` o, business b "
			+ "where o.stall = b.stall_id and o.book_time like '%${today}%' "
			+ "and b.boss_id = #{boss_id} "
			+ "and o.state != 0 group by o.number") 
	public List<String> findOrders2(String boss_id, String today);
	//获取订单号（本月订单）
	@Select("select o.number "
			+ "from `order` o, business b "
			+ "where o.stall = b.stall_id and o.book_time like '%${month}%' "
			+ "and b.boss_id = #{boss_id} "
			+ "and o.state != 0 group by o.number") 
	public List<String> findOrders3(String boss_id, String month);
	//商家身份验证
	@Select("select boss_id from business where boss_id = #{boss_id}")
	public String Authentication(String boss_id);
}
