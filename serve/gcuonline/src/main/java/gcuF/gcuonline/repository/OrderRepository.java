package gcuF.gcuonline.repository;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import gcuF.gcuonline.bean.Order;

public interface OrderRepository {
	//添加订单
	@Insert("insert into `order` (number, stall, id, p_id, price, num, total, state, book_time, get_time) "
			+ "values(#{number},#{stall},#{id},#{p_id},#{price},#{num},#{total},#{state},#{book_time},#{get_time})")
	public int insertOrder(Order order);
	//获取最大订单号
	@Select("SELECT MAX(number) number FROM `order`")
	public int selectMax();
	//添加用户
	@Insert("insert into `user` (username, user_id) "
			+ "select #{username}, #{user_id} from `user` "
			+ "where not exists(select * from `user` where user_id = #{user_id}) limit 1")
	public String insertUser(String username, String user_id);
	//未出单-->已出单
	@Update("UPDATE `order` SET state = 2 WHERE number = #{number}")
	public Integer upDateOrderState1(int number);
	//已出单-->已收货
	@Update("UPDATE `order` SET state = 3, get_time = #{gettime} WHERE number = #{number}")
	public Integer upDateOrderState2(int number, String gettime);	
	//订单取消
	@Update("UPDATE `order` SET state = 0 WHERE number = #{number}")
	public Integer deleteOrderState(int number);
}
