package gcuF.gcuonline.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import gcuF.gcuonline.bean.Food;

public interface FoodRepository {
	@Insert("insert into food (stall,name,type,price,num,introduce,picture)"
			+ "value(#{stall},#{name},#{type},#{price},#{num},#{introduce},#{picture})")
	public int insertFood(Food food);
	
	@Select("select * from food where place=#{id}")
	public List<Food> findFoodByPlace(int id);
	
	@Select("select * from food")
	public List<Food> findFoodAll();
	
	@Select("select * from food where name like '%${value}%'")
	public List<Food> findFoodByName(String value);
	
	@Update("update stall set s_search = #{type} where s_id = #{stall}")
	public int updateStallForSearch(int stall,String type);
	
	@Select("select s_search from stall where s_id = #{stall}")
	public String findSearchByStallID(int stall);
	
	@Delete("delete from food where id = #{id}")
	public int deleteFoodById(int id);
	
	@Select("select count(*) from food where type = #{type} and stall=#{stall}")
	public int selectTypeNumByType(String type,int stall);
	
	@Select("select type from food where id=#{id}")
	public String selectTypeById(int id);
	
	@Select("select s_search from stall where s_id=#{id}")
	public String selectS_searchByStall(int id);
	
	@Select("select stall from food where id=#{id}")
	public int selectStallByFood(int id);
	//通过档口id删除菜品
	@Delete("delete from food where stall = #{id}")
	public int deleteFoodByStallId(int id);
	
	//通过id查找food
	@Select("select * from food where id =#{id} ")
	public Food selectFoodById(int id);
	
	@Update("update food set name=#{name},price=#{price},introduce=#{introduce},stall=#{stall},num=#{num} where id = #{id}")
	public Integer upDateFoodById(Food food);
}
