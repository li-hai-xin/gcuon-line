package gcuF.gcuonline.repository;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import gcuF.gcuonline.bean.Food;

public interface FoodTypeRepository {
	
	@Select("select * from food where stall=#{stall} and type=#{type}")
	public List<Food> findFoodsByStallAndType(int stall,String type);
	
	@Select("select type from food where stall=#{stall} group by type")
	public List<String> getFoodsType(int stall);
	
}
