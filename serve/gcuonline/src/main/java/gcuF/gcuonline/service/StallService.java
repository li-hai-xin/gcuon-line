package gcuF.gcuonline.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import gcuF.gcuonline.bean.Stall;
import gcuF.gcuonline.repository.FoodRepository;
import gcuF.gcuonline.repository.StallRepository;

@Service
public class StallService {
	@Resource
	private StallRepository stallRepository;
	@Resource 
	private FoodRepository foodRepository;
	
	public int insertStall(Stall stall) {
		return stallRepository.insertStall(stall);
	}
	public List<Stall> findStallByPlace(int id) {
		return stallRepository.findStallByPlace(id);
	}
	
	public List<Stall> findStallAll() {
		return stallRepository.findStallAll();
	}	
	public List<Stall> findStallsByFood(String value){
		return stallRepository.findStallsByFood(value);
	}
	
	public List<Stall> findStallsByName(String value){
		return stallRepository.findStallsByName(value);
	}
	
	public List<Stall> findStallsName(){
		return stallRepository.findStallsName();
	}
	
	public int deleteStallById(int id) {
		foodRepository.deleteFoodByStallId(id);
		System.out.println("hkjhkhkhh");
		return stallRepository.deleteStallById(id);
	}
	public Integer upDateStallById(Stall stall) {
		return stallRepository.upDateStallById(stall);
	}
	public Stall selectStallById(int id) {
		return stallRepository.selectStallById(id);
	}
}
