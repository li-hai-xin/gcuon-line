package gcuF.gcuonline.service;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import gcuF.gcuonline.bean.Order;
import gcuF.gcuonline.repository.OrderRepository;
import gcuF.gcuonline.tools.MyUtil;

@Service
public class OrderService {
	@Resource
	private OrderRepository orderRepository;
	@Resource
	private MyUtil myUtil;
	public int insertOrder(Order order) {
		order.total = order.price;
		BigDecimal b = new BigDecimal(order.price / (double)order.num);  
		order.price = b.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();
		String time = myUtil.getTime();
		order.book_time = order.get_time = time;
		order.state = 1;
		return orderRepository.insertOrder(order);
	}
	public String insertUser(String username, String user_id) {
		return orderRepository.insertUser(username, user_id);
	}
	public Integer upDateOrderState1(int number) {
		return orderRepository.upDateOrderState1(number);
	}
	public Integer upDateOrderState2(int number) {
		String gettime = myUtil.getTime();
		return orderRepository.upDateOrderState2(number, gettime);
	}
	public Integer deleteOrderState(int number) {
		return orderRepository.deleteOrderState(number);
	}
	public int selectMax() {
		return orderRepository.selectMax();
	}
}
