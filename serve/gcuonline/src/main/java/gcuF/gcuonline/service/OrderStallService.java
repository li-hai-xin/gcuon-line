package gcuF.gcuonline.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import gcuF.gcuonline.pojo.Orders;
import gcuF.gcuonline.repository.OrderStallRepository;
import gcuF.gcuonline.tools.MyUtil;

@Service
public class OrderStallService {
	@Resource
	private OrderStallRepository orderStallRepository;
	@Resource
	private MyUtil myUtil;
	
	public List<Orders> findOrdersByNumber(String number) {
		return orderStallRepository.findOrdersByNumber(number);
	}
	public List<Orders> findOrderForStallToday(String number) {
		return orderStallRepository.findOrderForStallToday(number);
	}
	public List<Orders> findOrderForStallMonth(String number) {
		return orderStallRepository.findOrderForStallMonth(number);
	}
	public List<String> findOrders1(String boss_id){
		return orderStallRepository.findOrders1(boss_id);
	}
	public List<String> findOrders2(String boss_id){
		String today = myUtil.getDay();
		return orderStallRepository.findOrders2(boss_id, today);
	}
	public List<String> findOrders3(String boss_id){
		String month = myUtil.getMonth();
		return orderStallRepository.findOrders3(boss_id, month);
	}
	public String Authentication(String boss_id) {
		String judge = orderStallRepository.Authentication(boss_id);
		if(judge == null) {
			judge = "false";
		}
		else {
			judge = "true";
		}
		return judge;
	}
}
