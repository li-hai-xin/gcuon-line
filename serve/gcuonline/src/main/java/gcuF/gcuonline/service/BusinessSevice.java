package gcuF.gcuonline.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import gcuF.gcuonline.bean.Business;
import gcuF.gcuonline.repository.BusinessRepository;

@Service
public class BusinessSevice {
	
	@Resource
	private BusinessRepository businessRepository;
	
	public int insertBoss(Business boss) {
		return businessRepository.insertBoss(boss);
	}
}
