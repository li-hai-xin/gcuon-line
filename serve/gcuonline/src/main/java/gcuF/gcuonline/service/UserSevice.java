package gcuF.gcuonline.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import gcuF.gcuonline.bean.User;
import gcuF.gcuonline.repository.UserRepository;

@Service
public class UserSevice {
	
	@Resource
	private UserRepository userRepository;
	
	public int insertUser(User user) {
		List<User> list = userRepository.isHave(user.getUser_id());
		if(list.size() == 1) {
			return 0;
		}else {
			return userRepository.insertUser(user);
		}
	}
	
	public List<User> findAll(){
		return userRepository.findAll();
	}
	
	public List<User> isHave(String user_id) {
		return userRepository.isHave(user_id);
	}
}
