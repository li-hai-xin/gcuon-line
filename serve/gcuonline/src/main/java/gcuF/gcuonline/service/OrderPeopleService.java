package gcuF.gcuonline.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import gcuF.gcuonline.pojo.OrderDatailed;
import gcuF.gcuonline.pojo.OrderPeople;
import gcuF.gcuonline.repository.OrderPeopleRepository;

@Service
public class OrderPeopleService {
	@Resource
	private OrderPeopleRepository orderPeopleRepository;
	
	public List<OrderPeople> findOrderForPeopleNotissued(String number) {
		return orderPeopleRepository.findOrderForPeopleNotissued(number);
	}
	public List<OrderPeople> findOrderForPeopleIssued(String number) {
		return orderPeopleRepository.findOrderForPeopleIssued(number);
	}
	public List<OrderPeople> findOrderForPeopleReceived(String number) {
		return orderPeopleRepository.findOrderForPeopleReceived(number);
	}
	public List<String> findNumberForPeopleNotissued(String user_id){
		return orderPeopleRepository.findNumberForPeopleNotissued(user_id);
	}
	public List<String> findNumberForPeopleIssued(String user_id) {
		return orderPeopleRepository.findNumberForPeopleIssued(user_id);
	}
	public List<String> findNumberForPeopleReceived(String user_id) {
		return orderPeopleRepository.findNumberForPeopleReceived(user_id);
	}
	public List<OrderDatailed> detailedOrderInformation(int number) {
		return orderPeopleRepository.detailedOrderInformation(number);
	}
}
