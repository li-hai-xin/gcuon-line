package gcuF.gcuonline.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import gcuF.gcuonline.tools.RemoveType;
import gcuF.gcuonline.tools.UpdateSearch;
import gcuF.gcuonline.bean.Food;
import gcuF.gcuonline.repository.FoodRepository;

@Service
public class FoodService {
	@Resource
	private FoodRepository foodRepository;
	@Resource
	private RemoveType removeType;
	@Resource
	private UpdateSearch updateSearch;
	
	public int insertFood(Food food) {
		return foodRepository.insertFood(food);
	}
	public List<Food> findFoodByPlace(int id) {
		return foodRepository.findFoodByPlace(id);
	}
	public List<Food> findFoodAll() {
		return foodRepository.findFoodAll();
	}
	public List<Food> findFoodByName(String name){
		return foodRepository.findFoodByName(name);
	}
	
	public int updateStallForSearch(int stall,String type) {
		String type1 = foodRepository.findSearchByStallID(stall);
		if(type1 == null) type1 = type;
		else type1 = type1 + "、"+type;
		System.out.println(type1);
		return foodRepository.updateStallForSearch(stall, type1);
	}
	public int deleteFoodById(int id) {
//		查看该菜品类型得数量，等于一的话，这个类型就删除
		int num = foodRepository.selectTypeNumByType(foodRepository.selectTypeById(id),foodRepository.selectStallByFood(id));
		System.out.println(num);
		if (num == 1) {
//			string为欲删除的s_search的子段
			String string = foodRepository.selectTypeById(id);
//			可能尾部需要去系列二字
			string = removeType.removeString(string);
			System.out.println(string);
			String s_search = foodRepository.selectS_searchByStall(foodRepository.selectStallByFood(id));
//			修改s_search
			String newSearch = updateSearch.newS_Search(s_search,string);
			foodRepository.updateStallForSearch(foodRepository.selectStallByFood(id), newSearch);
		}
		return foodRepository.deleteFoodById(id);
	}
	public Food selectFoodById(int id) {
		return foodRepository.selectFoodById(id);
	}
	
	public Integer upDateFoodById(Food food) {
		return foodRepository.upDateFoodById(food);
	}
}
