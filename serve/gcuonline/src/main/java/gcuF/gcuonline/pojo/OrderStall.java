package gcuF.gcuonline.pojo;

import java.util.List;

public class OrderStall {
	public int number;
	public String username;
	public List<Foods> foods;
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public List<Foods> getFoods() {
		return foods;
	}
	public void setFoods(List<Foods> foods) {
		this.foods = foods;
	}
}
