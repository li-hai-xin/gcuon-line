package gcuF.gcuonline.bean;

import java.io.Serializable;

//档口类
public class Stall implements Serializable{
	public int s_id;
	public String s_name;
	public String s_place;
	public String s_search;
	public String s_boss;
	public String s_phone;
	public double s_consume;
	public String s_introduce;
	public String s_time;
	public String s_picture;
	

	public String getS_picture() {
		return s_picture;
	}
	public void setS_picture(String s_picture) {
		this.s_picture = s_picture;
	}
	public int getS_id() {
		return s_id;
	}
	public void setS_id(int s_id) {
		this.s_id = s_id;
	}
	public String getS_name() {
		return s_name;
	}
	public void setS_name(String s_name) {
		this.s_name = s_name;
	}
	public String getS_place() {
		return s_place;
	}
	public void setS_place(String s_place) {
		this.s_place = s_place;
	}
	public String getS_search() {
		return s_search;
	}
	public void setS_search(String s_search) {
		this.s_search = s_search;
	}
	public String getS_boss() {
		return s_boss;
	}
	public void setS_boss(String s_boss) {
		this.s_boss = s_boss;
	}
	public String getS_phone() {
		return s_phone;
	}
	public void setS_phone(String s_phone) {
		this.s_phone = s_phone;
	}
	public double getS_consume() {
		return s_consume;
	}
	public void setS_consume(double s_consume) {
		this.s_consume = s_consume;
	}
	public String getS_introduce() {
		return s_introduce;
	}
	public void setS_introduce(String s_introduce) {
		this.s_introduce = s_introduce;
	}
	public String getS_time() {
		return s_time;
	}
	public void setS_time(String s_time) {
		this.s_time = s_time;
	}
}
