package gcuF.gcuonline.bean;

import java.io.Serializable;

//菜品实体
public class Food implements Serializable {
	public int id;
	public int stall;
	public String name;
	public String type;
	public double price;
	public int num;
	public String introduce;
	public String picture;
	
	public int getStall() {
		return stall;
	}
	public void setStall(int stall) {
		this.stall = stall;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getIntroduce() {
		return introduce;
	}
	public void setIntroduce(String introduce) {
		this.introduce = introduce;
	}
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
}
