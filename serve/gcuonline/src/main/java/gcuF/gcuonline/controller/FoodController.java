package gcuF.gcuonline.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gcuF.gcuonline.bean.Food;
import gcuF.gcuonline.service.FoodService;

@CrossOrigin
@RestController
@RequestMapping("/food")
public class FoodController {
	@Resource
	private FoodService foodService;
	@RequestMapping("/insertFood")
	public String insertFood(Food food) {
		return "插入【"+foodService.insertFood(food)+"】数据";
	}
	@RequestMapping("/findFoodByStall")
	public List<Food> findFoodByPlace(int id) {
		return foodService.findFoodByPlace(id);
	}
	@RequestMapping("/findFoodAll")
	public List<Food> findFoodAll() {
		return foodService.findFoodAll();
	}
	@RequestMapping("/updateStallForSearch")
	public int updateStallForSearch(int stall,String type) {
		return foodService.updateStallForSearch(stall, type);
	}
	@RequestMapping("/findFoodByName")
	public List<Food> findFoodByName(String name){
		return foodService.findFoodByName(name);
	}
	@RequestMapping("/deleteFoodById")
	public int deleteFoodById(int id) {
		return foodService.deleteFoodById(id);
	}
	@RequestMapping("/selectFoodById")
	public Food selectFoodByID(int id) {
		return foodService.selectFoodById(id);
	}
	
	@RequestMapping("/upDateFoodById")
	public Integer upDateStallById(Food food) {
		return foodService.upDateFoodById(food);
	}
}
