package gcuF.gcuonline.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gcuF.gcuonline.bean.Business;
import gcuF.gcuonline.service.BusinessSevice;

@CrossOrigin
@RestController
@RequestMapping("/business")
public class BusinessController {
	@Resource
	private BusinessSevice businessSevice;
	@RequestMapping("/insertBoss")
	public String insertBoss(Business boss) {
		return "插入【"+businessSevice.insertBoss(boss)+"】数据";
	}
}
