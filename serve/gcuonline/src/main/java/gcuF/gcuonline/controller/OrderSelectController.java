package gcuF.gcuonline.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gcuF.gcuonline.pojo.Foods;
import gcuF.gcuonline.pojo.OrderStall;
import gcuF.gcuonline.pojo.Orders;
import gcuF.gcuonline.service.OrderStallService;

@RestController
@RequestMapping("/OrderStall")
public class OrderSelectController {
	@Resource
	private OrderStallService orderStallService;
	//商家查看订单（商家-未出单）
	@RequestMapping("/findOrdersByNumber")
	public List<OrderStall> findOrdersByNumber(String boss_id) {
		List<OrderStall> ordersStall = new ArrayList<OrderStall>();
		List<String> number = orderStallService.findOrders1(boss_id);
		for (int i = 0; i < number.size(); i++) {
			List<Orders> orders = orderStallService.findOrdersByNumber(number.get(i));
			List<Foods> foods = new ArrayList<Foods>();
			OrderStall orderStall = new OrderStall();
			for (int j = 0; j < orders.size(); j++) {
				Foods food = new Foods();
				orderStall.setNumber(orders.get(j).getNumber());
				orderStall.setUsername(orders.get(j).getUsername());
				food.setNum(orders.get(j).getNum());
				food.setName(orders.get(j).getName());
				food.setPicture(orders.get(j).getPicture());
				foods.add(food);
				orderStall.setFoods(foods);
			}
			ordersStall.add(orderStall);
		}
		return ordersStall;
	}
	//查看订单（商家-今日订单）
	@RequestMapping("/findOrderForStallToday")
	public List<OrderStall> findOrderForStallToday(String boss_id) {
		List<OrderStall> ordersStall = new ArrayList<OrderStall>();
		List<String> number = orderStallService.findOrders2(boss_id);
		for (int i = 0; i < number.size(); i++) {
			List<Orders> orders = orderStallService.findOrderForStallToday(number.get(i));
			List<Foods> foods = new ArrayList<Foods>();
			OrderStall orderStall = new OrderStall();
			for (int j = 0; j < orders.size(); j++) {
				Foods food = new Foods();
				orderStall.setNumber(orders.get(j).getNumber());
				orderStall.setUsername(orders.get(j).getUsername());
				food.setNum(orders.get(j).getNum());
				food.setName(orders.get(j).getName());
				food.setPicture(orders.get(j).getPicture());
				foods.add(food);
				orderStall.setFoods(foods);
			}
			ordersStall.add(orderStall);
		}
		return ordersStall;
	}
	//查看订单（商家-本月订单）
	@RequestMapping("/findOrderForStallMonth")
	public List<OrderStall> findOrderForStallMonth(String boss_id) {
		List<OrderStall> ordersStall = new ArrayList<OrderStall>();
		List<String> number = orderStallService.findOrders3(boss_id);
		for (int i = 0; i < number.size(); i++) {
			List<Orders> orders = orderStallService.findOrderForStallMonth(number.get(i));
			List<Foods> foods = new ArrayList<Foods>();
			OrderStall orderStall = new OrderStall();
			for (int j = 0; j < orders.size(); j++) {
				Foods food = new Foods();
				orderStall.setNumber(orders.get(j).getNumber());
				orderStall.setUsername(orders.get(j).getUsername());
				food.setNum(orders.get(j).getNum());
				food.setName(orders.get(j).getName());
				food.setPicture(orders.get(j).getPicture());
				foods.add(food);
				orderStall.setFoods(foods);
			}
			ordersStall.add(orderStall);
		}
		return ordersStall;
	}
	//商家身份验证
	@RequestMapping("/Authentication")
	public String Authentication(String boss_id) {
		return orderStallService.Authentication(boss_id);
	}
	
}
