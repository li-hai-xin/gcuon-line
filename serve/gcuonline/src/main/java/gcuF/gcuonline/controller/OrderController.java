package gcuF.gcuonline.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import gcuF.gcuonline.bean.Order;
import gcuF.gcuonline.service.OrderService;

@CrossOrigin
@RestController
@RequestMapping("/Order")
public class OrderController {
	@Resource
	private OrderService OrderService;
	
	@ResponseBody
	@RequestMapping("/insertOrder")
	public String insertOrder(@RequestBody JSONObject jsonObject) {
		System.out.println("=====================");
	    System.out.println(jsonObject.toJSONString());
	    try {
	    	JSONObject jsonobject = jsonObject.getJSONObject("orderInfo");
	    	Long stall = jsonobject.getLong("stall");
	        String p_id = jsonobject.getString("p_id");
	        JSONArray orderingList = jsonobject.getJSONArray("orderingList");
	        JSONArray jsonArray = new JSONArray(orderingList);
	        Order order = new Order();
	        int number = OrderService.selectMax();
			order.number = number + 1;
	        for(int i = 0; i < jsonArray.size(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                Long num = object.getLong("num");
                double price = Double.parseDouble(object.getString("price"));
                Long id = object.getLong("id");
                order.setStall(stall);
    	        order.setP_id(p_id);
                order.setNum(num);
                order.setPrice(price);
                order.setId(id);
                insertOrder(order);
            }
	    }
	    catch (Exception e) {
	        e.printStackTrace();
	    }
	    return "新增了新订单！";
	}
	//添加订单
	public String insertOrder(Order order) {
		return "新增" + OrderService.insertOrder(order) + "条新订单";
	}
	//订单由未出单变为已出单
	@RequestMapping("/upDateOrderState1")
	public Integer upDateOrderState1(int number) {
		return OrderService.upDateOrderState1(number);
	}
	//订单由已出单变为已收货
	@RequestMapping("/upDateOrderState2")
	public Integer upDateOrderState2(int number) {
		return OrderService.upDateOrderState2(number);
	}
	//订单取消
	@RequestMapping("/deleteOrderState")
	public Integer deleteOrderState(int number) {
		return OrderService.deleteOrderState(number);
	}
}
