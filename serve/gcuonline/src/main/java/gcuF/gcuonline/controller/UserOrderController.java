package gcuF.gcuonline.controller;

import java.util.ArrayList;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gcuF.gcuonline.pojo.OrderDatailed;
import gcuF.gcuonline.pojo.OrderPeople;
import gcuF.gcuonline.service.OrderPeopleService;

@RestController
@RequestMapping("/OrderPeople")
public class UserOrderController {
	@Resource
	private OrderPeopleService orderPeopleService;
	//用户查看订单（未出单）
	@RequestMapping("/findOrderForPeopleNotissued")
	public List<OrderPeople> findOrderForPeopleNotissued(String user_id) {
		List<OrderPeople> orderPeople = new ArrayList<OrderPeople>();
		List<String> number = orderPeopleService.findNumberForPeopleNotissued(user_id);
		for (int i = 0; i < number.size(); i++) {
			List<OrderPeople> orders = orderPeopleService.findOrderForPeopleNotissued(number.get(i));
			OrderPeople orderpeople = new OrderPeople();
			double total = 0.0;
			for (int j = 0; j < orders.size(); j++) {
				orderpeople.setNumber(orders.get(j).getNumber());
				orderpeople.setBook_time(orders.get(j).getBook_time());
				total = total + orders.get(j).getTotal();
				orderpeople.setTotal(total);
				orderpeople.setS_name(orders.get(j).getS_name());
				orderpeople.setS_picture(orders.get(j).getS_picture());
			}
			orderPeople.add(orderpeople);
		}
		return orderPeople;
	}
	//用户查看订单（已出单）
	@RequestMapping("/findOrderForPeopleIssued")
	public List<OrderPeople> findOrderForPeopleIssued(String user_id) {
		List<OrderPeople> orderPeople = new ArrayList<OrderPeople>();
		List<String> number = orderPeopleService.findNumberForPeopleIssued(user_id);
		for (int i = 0; i < number.size(); i++) {
			List<OrderPeople> orders = orderPeopleService.findOrderForPeopleIssued(number.get(i));
			OrderPeople orderpeople = new OrderPeople();
			double total = 0.0;
			for (int j = 0; j < orders.size(); j++) {
				orderpeople.setNumber(orders.get(j).getNumber());
				orderpeople.setBook_time(orders.get(j).getBook_time());
				total = total + orders.get(j).getTotal();
				orderpeople.setTotal(total);
				orderpeople.setS_name(orders.get(j).getS_name());
				orderpeople.setS_picture(orders.get(j).getS_picture());
			}
			orderPeople.add(orderpeople);
		}
		return orderPeople;
	}
	//用户查看订单（已收货）
	@RequestMapping("/findOrderForPeopleReceived")
	public List<OrderPeople> findOrderForPeopleReceived(String user_id) {
		List<OrderPeople> orderPeople = new ArrayList<OrderPeople>();
		List<String> number = orderPeopleService.findNumberForPeopleReceived(user_id);
		for (int i = 0; i < number.size(); i++) {
			List<OrderPeople> orders = orderPeopleService.findOrderForPeopleReceived(number.get(i));
			OrderPeople orderpeople = new OrderPeople();
			double total = 0.0;
			for (int j = 0; j < orders.size(); j++) {
				orderpeople.setNumber(orders.get(j).getNumber());
				orderpeople.setBook_time(orders.get(j).getBook_time());
				total = total + orders.get(j).getTotal();
				orderpeople.setTotal(total);
				orderpeople.setS_name(orders.get(j).getS_name());
				orderpeople.setS_picture(orders.get(j).getS_picture());
			}
			orderPeople.add(orderpeople);
		}
		return orderPeople;
	}
	//查看详细菜品信息
	@RequestMapping("/detailedOrderInformation")
	public List<OrderDatailed> detailedOrderInformation(int number) {
		return orderPeopleService.detailedOrderInformation(number);
	}
}
