# 目标

第一次迭代目标：完成档口的创建以及菜品的添加和浏览

档口创建，添加菜单是在网页上实现的，而小程序实现数据的读取。

# 数据

没有使用对象存储服务，手动将图片变为网络资源
档口：http://p.ananas.chaoxing.com/star3/493_301Q50/0d1bca26342bcecb2b06ed761a704ce3.png?rw=493&rh=301&_fileSize=15035&_orientation=1

鸡扒：http://p.ananas.chaoxing.com/star3/460_279Q50/2ea0731ddd93faf18b216b0186dd3e8c.png?rw=460&rh=279&_fileSize=24421&_orientation=1

酸梅汤：http://p.ananas.chaoxing.com/star3/origin/72a0d4bc05d7fec37f32bf0543f425a4.jpg?rw=500&rh=317&_fileSize=21259&_orientation=1

# 接口

## 查看档口

#### 1. 接口描述

输入数据创建档口，并保存到数据库

| 请求方式 | POST                                             |
| -------- | ------------------------------------------------ |
| 请求url  | http://localhost:8090/stall/findStallByPlace?id= |
| 具体功能 | 创建档口                                         |

#### 2. 参数说明

| 参数名 | 是否必传 | 参数描述         | 枚举值列表 |
| ------ | -------- | ---------------- | ---------- |
| id     | 是       | 档口所在食堂的id | 1、2、3、4 |

#### 3. 返回结构

```
{
  "code": 200,
  "message": "success",
  "data": {
		s_id:5
		s_name:"蒸功夫"
		s_place:"2"
		s_search:"蒸功夫、汤包、肉包、菜包、蒸饺"
		s_boss:"麦耀钟"
		s_phone:"2008611"
		s_consume:35.5
		s_introduce:"众所周知，蒸功夫无敌，你tm值得拥有"
		s_time:null
		s_picture:"http://p.ananas.chaoxing.com/star3/origin/5c4268f7168e601ef3395ff4ee420acf.jpg?rw=800&rh=442&_fileSize=74922&_orientation=1"
  }
}
{
  "code": 400,
  "message": "fail",
  "data": {
  }
}
```
